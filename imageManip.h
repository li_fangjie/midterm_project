// Sabrina Li
// sli159
// Fangjie Li
// fli35
#ifndef _PPM_PROC_H_
#define _PPM_PROC_H_

#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "ppm_io.h"

/*  ------- Some helper functions for pixel(pix) manipulation ------- */

// Get a particular pix at a location, returns NULL if out of bound.
Pixel * get_pix(Image * img, int col, int row); 

// Get a pix a relative location from another pix, returns NULL if out of bound.
Pixel * get_pix_d(Image * img, int cur_col, int cur_row, int d_col, int d_row);

// Get a pix a relative location from another pix, returns NULL if out of bound.
Pixel * get_pix_i_d(Image * img, int cur_i, int d_col, int d_row);

// Get the length of data array of an image.
int get_d_len(Image * img);

//Get red value for a pixel
int get_r(Image * img, int col, int row);

//Get blue value for a pixel
int get_b(Image * img, int col, int row);

//Get green value for a pixel
int get_g(Image * img, int col, int row);

//Set red value for a pixel
void set_r(Image * img, int col, int row, int val);

//Set blue value for a pixel
void set_b(Image * img, int col, int row, int val);

//Set green value for a pixel
void set_g(Image * img, int col, int row, int val);

void set_rbg(Pixel *data, int red, int blue, int val);
/*  ------- Actual functions for different effects -------
NOTE: All functions should follow the following guidlines:
- Function names should be as in the operation names in
  http://cs.jhu.edu/~darvish/cs220/midtermcs220#description
- functions arguments should be in the form of:
  foo(Image * o_img, Image * i_img(_1, Image * i_img_2...), float coef(_1, float coef_2...))
- functions should have int as output to indicate error state.
*/

int exposure(Image * o_img, Image * i_img, float coef);

int a_blend(Image * o_img, Image * i_img_1, Image * i_imig_2, float coef);

int zoom_in(Image * o_img, Image * i_img);

int zoom_out(Image * o_img, Image * i_img);

void new_pt(Image * o_img, int rad, int * coord);

int pointilism(Image * o_img, Image * i_img);

Pixel *getSwirlPix(Image * i_img, double c_col, double c_row, int x, int y, int scale);

int swirl(Image * o_img, Image * i_img, int c_col, int c_row, int scale);

double get_gauss(int d_x, int d_y, float coef);

void fill_gauss(double *matrix, int size, float coef);

void blur_pix(Pixel * pix, int col, int row, Image * i_img, int size, int h_size,  double * matrix, int * c_b, int * r_b);

int blur(Image * o_img, Image * i_img, float coef);


//Functions that handle command line and check error codes
void clear (FILE *pointers[], int p, Image *images[], int im);

int clear_error(FILE * pointers[], int p, Image * images[], int im, char * err, int num);

int run_imageManip(int argc, char * argv[]);
#endif

