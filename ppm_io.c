// Sabrina Li
// sli159
// Fangjie Li
// fli35

// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ppm_io.h"
#include "imageManip.h"

/*
int read_ppm(FILE *fp, Image * img) 
{
	printf("Reading ppm file...\n");
	// check that fp is not NULL
	if (fp == NULL)
		return 2;
	if (img == NULL)
		return 8;
	// Start reading the meta data, raise error if not pmm file
	char buf[3] = {0}; // Buffer for "p6"
	fscanf(fp, "%s ", buf);
	if (toupper(buf[0]) != 'P' || buf[1] != '6'){
		printf("NOT PPM!\n %d %d \n", buf[0], buf[1]);
		printf("wat1\n");
		return 3;
	}
	int i = 0;	
	// Loops to read cols, rows. Discards any comment(s) and Colors.
	// Note: this can support discarding multiple lines of comment
	// Not required, but should be fine.
	while(i < 3){
		char cur_c  = fgetc(fp);
		if(cur_c == '#'){
			while(cur_c != 10)
				cur_c = fgetc(fp);
		} else {
			ungetc(cur_c, fp);
			if(i < 2){ // Reads in cols and rows. Raise format error if invalid
				if(1 != fscanf(fp, "%d ", img -> sz + i)){
					printf("wat ????%d\n", i);
					printf("Error reading row: \n");//, *(img -> sz+i));
					return 3;
				}
			} else{
				int temp; 
				// reads in colors but not stored. Raise format error if failed
				if(1 != fscanf(fp, "%d ", &temp)){
					printf("Error reading colours :%d\n", temp);
					printf("wat3\n");
					return 3;
				}
			}
			++i;
		}
	}
	// If the size of image is not correct, the file is not properly 
	if(img -> sz[0] < 1 || img -> sz[1] < 1){
		printf("wrong img size???\n");
		return 3; 
	}
	// Set data of img to be size of ppm file.
	if(0 != init_data(img, img -> sz[0], img -> sz[1]))
		return 8;
	// int len = img -> sz[0] * img -> sz[1];
	// read all data from ppm:
	int len = get_d_len(img); 
	int act_len = (int)fread(img -> data, sizeof(Pixel), len, fp);
	if(len != act_len){
		printf("Image length should be %d, but read %d!!\n", len, act_len);
		return 3;
	}
	return 0;  
}
*/

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
int read_ppm(FILE *fp, Image * img) 
{
	printf("Reading PPM file...\n");
	// check that fp is not NULL
	if (fp == NULL)
		return 2;
	if (img == NULL)
		return 8;
	// Start reading the meta data, raise error if not pmm file
	char head_c = fgetc(fp);
	int head_n  = 0;
	fscanf(fp, "%d ", &head_n);
	if (head_c != 'P' || head_n != 6){
		printf("PPM \"Magic Number\" Format Error. Scanned: \"%c%d\", Should be \"P6\"\n", head_c, head_n);
		return 3;
	}
	// Loops to read cols, rows. Discards any comment(s) and Colors.
	// Note: this can support discarding multiple lines of comment
	// Not required, but should be fine.
	char cur_c  = fgetc(fp);
	if(cur_c == '#'){
		fscanf(fp, "%*[^\n]"); // Ignore comment.
	} else if (isdigit(cur_c)){
		ungetc(cur_c, fp);
	} else {
		printf("PPM format error, unexpected, none comment character after \"P6\"\n");
		return 3;
	}
	int color; 
	// Reads in cols rows and colors. Raise format error if invalid
	if(3 != fscanf(fp, " %d %d %d", img -> sz, img -> sz+1, &color)){
		printf("PPM Row/Col/Colour Fromat Error, read: \"%d, %d, %d\"\n", img -> sz[0], img -> sz[1], color);
		return 3;
	}
	cur_c  = fgetc(fp);
	if(!isspace(cur_c)){
		printf("Non newline/space character after colour: %d\n", cur_c);
		return 3;
	}
	// If the size of image is not correct, or if the colour size is wrong, the file is not correct 
	if(img -> sz[0] < 1 || img -> sz[1] < 1 || color != 255){
		printf("wrong img size/color. Scanned: %d %d %d\n", img->sz[0], img->sz[1], color);
		return 3; 
	}
	// Set data of img to be size of ppm file.
	if(0 != init_data(img, img -> sz[0], img -> sz[1]))
		return 8;
	// int len = img -> sz[0] * img -> sz[1];
	// read all data from ppm:
	int len = get_d_len(img);
	printf("Len: %d", len);
	int act_len = (int)fread(img -> data, sizeof(Pixel), len, fp);

	///check checks if the number of pixels available to be read is larger than specified sizes
	int check = (int) fread(img->data, sizeof(Pixel),1,fp);
	if (check!=0)
	  {
	    return 3;
	  }
	printf("Actual length: %d", act_len);
	if(len != act_len){
		printf("Image length should be %d, but read %d!!\n", len, act_len);
		return 3;
	}
	return 0;  
}

// Creates a new Image, WITHOUT allocating memory for data, which is set to NULL.
// Returns NULL if allocation fails.
Image * new_img()
{
	Image * img = calloc(1, sizeof(Image));
	if (img == NULL)
		return NULL;
	img -> data = NULL; // To prevent free() with non-initialised pointer
	return img;
}

// Creates a new Image, and allocates memory for data.
// Returns NULL if allocation for Image or data fails.
Image * init_img(int col, int row)
{
	Image * img = new_img();
	if (img == NULL)
		return NULL;
	if(0 != init_data(img, col, row)){
		free(img);
		return NULL;
	}
	return img;
}

// Deep copy all data and fields from src to des.
// Returns 8 if des can't have its data allocated properly
int cpy_img(Image * des, Image * src)
{	// In case alloc for data failed
	if (0 != init_data(des, src -> sz[0], src -> sz[1])) 
		return 8;
	// int len = src -> sz[0] * src -> sz[1];
	int len = get_d_len(src);
	memcpy(des -> data, src -> data, len * sizeof(Pixel));
	return 0;
}

// Completely frees memory held by an Image, including img and its data
int rmv_img(Image * img)
{
	free(img -> data);
	free(img);
	return 0;
}

// Allocates memory for an image, and defines its new size
// Returns 8 if failed somehow.
int init_data(Image * img, int col, int row)
{
	Pixel * n_data = calloc(col * row, sizeof(Pixel));
	if (n_data == NULL)
		return 8;
	if (img -> data != NULL) // Just in case the img given here already has data alloced.
		free(img -> data);
	img -> data = n_data;
	img -> sz[0] = col;
	img -> sz[1] = row;
	return 0;
}


/* Write a PPM-formatted image to a file (assumes fp != NULL) */
int write_ppm(FILE *fp, const Image *im) 
{
	printf("Writing output PPM file...\n");
	// check that fp is not NULL
	if (fp == NULL){
		fprintf(stdout, "file is not properly opened!\n");
		return 7;
	}
	if (im == NULL)
	  return 8;
	// write PPM file header, in the following format
	// P6
	// sz[0] sz[1]
	// 255
	fprintf(fp, "P6\n%d %d\n255\n", im->sz[0], im->sz[1]);

	// now write the pixel array
	int num_pix = fwrite(im->data, sizeof(Pixel), im->sz[0] * im->sz[1], fp);
	if (num_pix != im->sz[0] * im->sz[1]) {
		printf("Pixel data failed to write properly!\n");
		return 7;
	}
	printf("PPM file Written.\n");
	
	return 0;
}

