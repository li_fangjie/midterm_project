// Sabrina Li
// sli159
// Fangjie Li
// fli35
#include <stdio.h>
#include <stdlib.h>
#include "imageManip.h"
#include "ppm_io.h"

int main(int argc, char * argv[])
{
  return(run_imageManip(argc, argv));
}
  /*
   if (argc<=1)
    return 1;
  
  FILE *input = fopen(argv[1], "rb"); 

  if (input == NULL)
    {
      return 2;
    }
  
  Image * i_img = new_img();
  int inputread = read_ppm(input,i_img);
  
  if(inputread == 0)
    {
      printf("The input img size is:\n%d cols. \n%d rows. \n", i_img->sz[0], i_img->sz[1]);
    }
  else
   {
      printf("Error creating Image object!! (1) \n code: %d\n", inputread);
      fclose(input);
      rmv_img(i_img);
      return inputread;
    }

  Image * o_img = new_img();

  if (o_img ==NULL)
    {
      return 8;
    }

  //Matching for name of specified function
  if (strcmp("exposure",argv[3])==0)
    {
      float a;
      int scanned = sscanf(argv[4], "%f", &a);
      if (scanned!=1 | a <-3 | a>3)
	{
	  return 6;
	}
      exposure(o_img, i_img, a);
    }
  else if (strcmp("a_blend",argv[3])==0)
    {
      FILE *input2 = fopen(argv[4], "rb");
      Image *i_img_2 = new_img();
      int secondinputread = read_ppm(input2,i_img_2);

      if (secondinputread==0)
	printf("The second img size is:\n%d cols. \n%d rows. \n", i_img->sz[0], i_img->sz[1]);
      else
	{
	  printf("Error creating image object!\n");
	  fclose(input2);
	  rmv_img(i_img_2);
	  fclose(input);
	  rmv_img(i_img);
	  return secondinputread;
	}
      float scale;
      sscanf(argv[5], "%f",&scale);
      a_blend(o_img, i_img,i_img_2, scale);
      fclose(input2);
      rmv_img(i_img_2);
    }
  else if (strcmp("zoom_in",argv[3])==0)
    {
      zoom_in(o_img,i_img);
    }
  else if (strcmp("zoom_out",argv[3])==0)
    {
      zoom_out(o_img,i_img);
    }
  else if (strcmp("pointilism",argv[3])==0)
    {
      pointilism(o_img,i_img);
    }
  else if (strcmp("swirl",argv[3])==0)
    {
      int c_col = 0;
      int c_row = 0;
      int scale = 0;
      sscanf(argv[4],"%d",&c_col);
      sscanf(argv[5],"%d",&c_row);
      sscanf(argv[6],"%d",&scale);
      swirl(o_img, i_img, c_col, c_row, scale);
    }
  else if(strcmp("blur",argv[3])==0)
    {
      float coef;
      sscanf(argv[4],"%f",&coef);
      blur(o_img, i_img, coef);
    }
  else
    {
      return 4;
      rmv_img(o_img);
      rmv_img(i_img);
      fclose(input);
    }
  
    FILE* outputwrite = fopen(argv[2],"wb");
    int written = write_ppm(outputwrite,o_img);
    if (written==0)
      {
	fclose(input);
	fclose(outputwrite);
	rmv_img(o_img);
	rmv_img(i_img);
	return written;
      }
    else
      printf("Wrote %d\n", written);
    fclose(input);
    fclose(outputwrite);
    rmv_img(o_img);
    rmv_img(i_img);*/


/*
int run_imageManip(int argc, char *argv[])
{
  if (argc<=1)
    return 1;
  
  FILE *input = fopen(argv[1], "rb"); 

  if (input == NULL)
    {
      return 2;
    }
  
  Image * i_img = new_img();
  int inputread = read_ppm(input,i_img);
  
  if(inputread == 0)
    {
      printf("The input img size is:\n%d cols. \n%d rows. \n", i_img->sz[0], i_img->sz[1]);
    }
  else
   {
      printf("Error creating Image object!! (1) \n code: %d\n", inputread);
      fclose(input);
      rmv_img(i_img);
      return inputread;
    }

  Image * o_img = new_img();

  if (o_img ==NULL)
    {
      return 8;
    }

  //Matching for name of specified function
  if (strcmp("exposure",argv[3])==0)
    {
      float a;
      int scanned = sscanf(argv[4], "%f", &a);
      if ((scanned!=1) | (a <-3) | (a>3))
	{
	  return 6;
	}
      exposure(o_img, i_img, a);
    }
  else if (strcmp("a_blend",argv[3])==0)
    {
      FILE *input2 = fopen(argv[4], "rb");
      Image *i_img_2 = new_img();
      int secondinputread = read_ppm(input2,i_img_2);

      if (secondinputread==0)
	printf("The second img size is:\n%d cols. \n%d rows. \n", i_img->sz[0], i_img->sz[1]);
      else
	{
	  printf("Error creating image object!\n");
	  fclose(input2);
	  rmv_img(i_img_2);
	  fclose(input);
	  rmv_img(i_img);
	  return secondinputread;
	}
      float scale;
      sscanf(argv[5], "%f",&scale);
      a_blend(o_img, i_img,i_img_2, scale);
      fclose(input2);
      rmv_img(i_img_2);
    }
  else if (strcmp("zoom_in",argv[3])==0)
    {
      zoom_in(o_img,i_img);
    }
  else if (strcmp("zoom_out",argv[3])==0)
    {
      zoom_out(o_img,i_img);
    }
  else if (strcmp("pointilism",argv[3])==0)
    {
      pointilism(o_img,i_img);
    }
  else if (strcmp("swirl",argv[3])==0)
    {
      int c_col = 0;
      int c_row = 0;
      int scale = 0;
      sscanf(argv[4],"%d",&c_col);
      sscanf(argv[5],"%d",&c_row);
      sscanf(argv[6],"%d",&scale);
      swirl(o_img, i_img, c_col, c_row, scale);
    }
  else if(strcmp("blur",argv[3])==0)
    {
      float coef;
      sscanf(argv[4],"%f",&coef);
      blur(o_img, i_img, coef);
    }
  else
    {
      return 4;
      rmv_img(o_img);
      rmv_img(i_img);
      fclose(input);
    }
  
    FILE* outputwrite = fopen(argv[2],"wb");
    int written = write_ppm(outputwrite,o_img);
    if (written==0)
      {
	fclose(input);
	fclose(outputwrite);
	rmv_img(o_img);
	rmv_img(i_img);
	return written;
      }
    else
      printf("Wrote %d\n", written);

    fclose(input);
    fclose(outputwrite);
    rmv_img(o_img);
    rmv_img(i_img);
 
    }*/


/*********** TEST ***********
	int i = 0;
	FILE * temp = fopen("results/building-pointilism.ppm", "rb");
	Image * temp_i = new_img(); 
	read_ppm(temp, temp_i);
	for(int row=0; row < temp_i -> sz[1]; ++row){
		for(int col=0; col < temp_i -> sz[0]; ++col){
			Pixel * t_pix = get_pix(temp_i, col, row);
			Pixel * o_pix = get_pix(o_img, col, row);
			if(t_pix -> r != o_pix -> r || 
			t_pix -> g != o_pix -> g ||
			t_pix -> b != o_pix -> b){
				printf("**** Pixel at COL: %d, ROW: %d **** \n TEMPLATE || OUT \nR: %d || %d \nG: %d || %d \nB: %d || %d\n\n", 
				col, row, 
				t_pix -> r, o_pix -> r, 
				t_pix -> g, o_pix -> g,
				t_pix -> b, o_pix -> b);
				++i;
			}
		}
	}
	printf("%d\n", i);
	*********** END ***********/

/*
int main()
{
	 FILE * n_f = fopen("data/building.ppm", "rb");

        FILE * n_f = fopen("data/trees.ppm", "rb");
	Image * i_img = new_img();
	int r = read_ppm(n_f, i_img);
	if(r == 0){
		printf("The img size is:\n%d cols. \n%d rows. \n", i_img->sz[0], i_img->sz[1]);
	} else {
		printf("Error creating Image object!!\n");
		fclose(n_f);
		rmv_img(i_img);
		return 1;
	}
		
	for(int i=0; i<i_img->sz[1]; ++i){
		for(int j=0; j<i_img->sz[0]; ++j){
			Pixel * cur_pix = &(i_img->data[i_img -> sz[0] * i + j]);
			if(cur_pix -> r >  100){
				cur_pix -> r = 0;
				cur_pix -> g = 0;
				cur_pix -> b = 0;
			}
		//	printf("| %d %d %d |", cur_pix.r, cur_pix.g, cur_pix.b);
		}
		//printf("\n");
		}
	
	// For valgrind test: Run each function to check
	
	Image * o_img = new_img();
	exposure(o_img, i_img, 1.5);
	zoom_in(o_img, i_img);
	pointilism(o_img, i_img);
	blur(o_img, i_img, 2);
	FILE * o_f = fopen("out_2.ppm", "wb");	
	write_ppm(o_f, o_img);
	fclose(o_f);
	fclose(n_f);
	rmv_img(i_img);
	rmv_img(o_img);


	
*/
