// Sabrina Li
// sli159
// Fangjie Li
// fli35
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "ppm_io.h"
#include "imageManip.h"
#define PI 3.14159265358979323846

// Get a particular pix at a location, returns NULL if out of bound.
Pixel * get_pix(Image * img, int col, int row)
{
	if(col >= img -> sz[0] || row >= img -> sz[1] || col < 0 || row < 0){
		return NULL; // If out of bound, returns NULL 
	}
	return &(img -> data[row * img -> sz[0] + col]);
}

// Get a pix a relative location from another pix, returns NULL if out of bound.
Pixel * get_pix_d(Image * img, int cur_col, int cur_row, int d_col, int d_row)
{
	int new_col = cur_col + d_col;
	int new_row = cur_row + d_row;
	if(new_col >= img -> sz[0] || new_row >= img -> sz[1] || new_col < 0 || new_row < 0){
		return NULL; // If out of bound, returns NULL 
	}
	return &(img -> data[new_row * img -> sz[0] + new_col]);
}

Pixel * get_pix_i_d(Image * img, int cur_i, int d_col, int d_row)
{
	int new_i = d_row * img -> sz[0] + cur_i + d_col;
	int new_col = (cur_i % img ->sz[0]) + d_col;
	if(new_i >= get_d_len(img) || new_i < 0 || new_col<0 || new_col>=img->sz[0]){
		return NULL;
	}
	return &(img -> data[new_i]);
}

//Get red value for a pixel
int get_r(Image * img, int col, int row)
{
  return(get_pix(img,col,row)->r);
}

//Get blue value for a pixel
int get_b(Image * img, int col, int row)
{
  return(get_pix(img,col,row)->b);
}

//Get green value for a pixel
int get_g(Image * img, int col, int row)
{
  return(get_pix(img,col,row)->g);
}

//Set red value for a pixel
void set_r(Image * img, int col, int row, int val)
{
  get_pix(img,col,row)->r = val;
}

//Set blue value for a pixel
void set_b(Image * img, int col, int row, int val)
{
  get_pix(img,col,row)->b = val;
}

//Set green value for a pixel
void set_g(Image * img, int col, int row, int val)
{
  get_pix(img,col,row)->g = val;
}

//Set all three colors for a pixel at a given row, col in Pixel array
void set_rbg(Pixel *data, int red, int blue, int green)
{
  data->r = red;
  data->b = blue;
  data->g = green;
}
// Get the length of data array of an image.
int get_d_len(Image * img)
{ 
	return img -> sz[0] * img -> sz[1];
}


//IMAGE MANIP FUNCTIONS AND HELPERS:


int exposure(Image * o_img, Image * i_img, float coef)
{	
	// Copy over the content of i_img to o_img 
	if (0 != cpy_img(o_img, i_img))
		return 1;
	// int len = i_img -> sz[0] * i_img -> sz[1];
	int len = get_d_len(o_img);
	Pixel * cur_pix;
	for(int i=0; i<len; ++i){ // Iterates through each pixel
		cur_pix = &(o_img -> data[i]);
		unsigned char * arr[3] = {&cur_pix -> r, &cur_pix -> g, &cur_pix -> b};
		for(int j=0; j<3; ++j){ // Loops through r,g,b
			int  temp = (*arr[j] * pow(2, coef)); // int used bc result may > 255
			if (temp > 255) // In case final val > 255
				temp = 255;
			*arr[j] = (unsigned char)temp;
		}
		
	}
	return 0;
}

int a_blend(Image * o_img, Image * i_img_1, Image * i_img_2, float coef)
{
  //Check that images are not null
  if (o_img ==NULL || i_img_1 == NULL || i_img_2 == NULL)
    {
      printf("NULL");
      return 8;
    }
             //Determine size of output array that must be filled by finding the bigger of row and col between image 1 and image 2
  int outputrow;
  int outputcolumn;

  //Set outputcolumn
  if (i_img_1 ->sz[0] > i_img_2 -> sz[0])
    {
      outputcolumn = i_img_1->sz[0];
    }
  else
    {
      outputcolumn = i_img_2->sz[0];
    }
  //Set outputrow
  if (i_img_1->sz[1] > i_img_2 ->sz[1])
    {
      outputrow = i_img_1->sz[1];
    }
  else
    {
      outputrow = i_img_2->sz[1];
    }

  //new data array for o_img with size shrunken to fit output/blending of two images
  Pixel * resized = malloc(sizeof(Pixel)*outputrow*outputcolumn);
  if (resized==NULL)
    return 8;

  for (int i = 0; i <outputrow; i++)
    {
      for (int j = 0; j<outputcolumn; j++)
	{
	  if (j <i_img_1->sz[0] && i <i_img_1->sz[1]) //Check if this pixel exists within Image 1
	    {
	      if (j < i_img_2->sz[0] && i<i_img_2->sz[1]) //Checks if pixel is also within Image 2
		{
		  int r = coef * get_r(i_img_1,j,i) + (1-coef)*get_r(i_img_2,j,i);
		  int b = coef * get_b(i_img_1,j,i) + (1-coef)*get_b(i_img_2,j,i);
		  int g = coef * get_g(i_img_1,j,i) + (1-coef)*get_g(i_img_2,j,i);
		  set_rbg(&resized[i*outputcolumn+j],r,b,g);
       	       	}
	      else
		{ //Pixel only exists within Image 1, set output pixel to Image 1 pixel
		  resized[i*outputcolumn + j] = *(get_pix(i_img_1,j,i)); 
		}
	    }
	  else
	    {//Pixel only exists within Image 2, set output pixel to Image 2 pixel
	      resized[i*outputcolumn+j] = *(get_pix(i_img_2,j,i)); 
	    }
	}
     }
  o_img->data=resized;
  o_img->sz[0] = outputcolumn;
  o_img->sz[1] = outputrow;
  return 0;
  
}

int zoom_in(Image * o_img, Image * i_img)
{
	int coef = 2;
	if (0 != init_data(o_img, i_img -> sz[0] * 2, i_img -> sz[1] * 2)){
		printf("data alloc failed for zoom in\n");
		return 8;
	}
	for(int i=0; i < o_img -> sz[0]; ++i){
		for(int j=0; j < o_img -> sz[1]; ++j){
			*get_pix(o_img, i, j) = *get_pix(i_img, i / coef, j / coef);
		}
	}
	return 0;
}

int zoom_out(Image * o_img, Image * i_img)
{
  if (o_img==NULL || i_img==NULL)
    {
      return 8;
    }
  //Calc size of output image
  int outputcol = (i_img->sz[0])/2;
  int outputrow = (i_img->sz[1])/2;

  //Create new data array of pixels with correct output size
  Pixel * resized = malloc(sizeof(Pixel) * outputrow * outputcol);
  if (resized == NULL)
    return 8;

  //Loop through and calculate averages of old pixels for each new output pixel
  for (int i = 0; i<outputrow; i++)
    {
      for (int j = 0; j<outputcol; j++)
	{//Calculate avg red, blue, and green based on 2 block square originating from and including (i,j); aka square of (i,j), (i+1,j), (i,j+1), (i+1,j+1)
	  int avgr = (get_r(i_img,2*j, 2*i) + get_r(i_img,2*j+1, 2*i) + get_r(i_img,2*j, 2*i+1) + get_r(i_img,2*j+1,2*i+1))/4;
	  int avgb = (get_b(i_img,2*j, 2*i) + get_b(i_img,2*j+1, 2*i) + get_b(i_img,2*j, 2*i+1) + get_b(i_img,2*j+1,2*i+1))/4;
	  int avgg = (get_g(i_img,2*j, 2*i) + get_g(i_img,2*j+1, 2*i) + get_g(i_img,2*j, 2*i+1) + get_g(i_img,2*j+1,2*i+1))/4;
	  set_rbg(&resized[i*outputcol+j], avgr,avgb,avgg);
	}
    }
  //Set output image to correct data and dimensions
  o_img->data = resized;
  o_img->sz[0] = outputcol;
  o_img->sz[1] = outputrow;
  return 0;
  
}

// Creates a new "point" of radius=rad at coord[2] for pointilism
void new_pt(Image * o_img, int rad, int * coord)
{
	for(int d_x=-rad; d_x < rad + 1; ++d_x){ // Loops through each relative col
		int d_y_max = (int)(sqrt(pow(rad, 2) - pow(d_x, 2))); // Circle formula
		for (int d_y=-d_y_max; d_y < d_y_max + 1; ++d_y){ // Loops through rows
			Pixel * u_p = get_pix_d(o_img, coord[0], coord[1], d_x, d_y);
			if(u_p != NULL) // If not out of bound, replace its value to that of center
				* u_p = * get_pix(o_img, coord[0], coord[1]);
		}	
	}
}

int pointilism(Image * o_img, Image * i_img)
{ // Coefficients defined for easier testing.
	// srand(time(0));
	float pt_perc = 0.03;
	int pt_r = 5;
	if (0 != cpy_img(o_img, i_img))
		return 8;
	int pt_ct = get_d_len(o_img) * pt_perc;
	while(pt_ct > 0){
		int coord[2] = {rand() % (o_img -> sz[0]), rand() % (o_img -> sz[1])};
		// int col = rand() % (o_img -> sz[0]);
		// int row = rand() % (o_img -> sz[1]);
		// int coord[2] = {col, row};
		int r = rand() % pt_r + 1;
		// printf("%d %d %d\n", coord[0], coord[1], r);
		new_pt(o_img, r, coord); // For each center, generates a point.
		--pt_ct;
	}
	return 0;
}

//Calculates location of corresponding old pixel based on new pixel
Pixel * getSwirlPix(Image *i_img, double c_col, double c_row, int x, int y, int scale)
{
  //Calc alpha
  double a = (sqrt((x-c_col)*(x-c_col) + (y-c_row)*(y-c_row)))/ (scale);
  //Calc col and row of corresponding old pixel based on new pixel location
  int col = (int) (((x-c_col)*cos(a)) - ((y-c_row)*sin(a)) + c_col);
  int row = (int) (((x-c_col)*sin(a)) + ((y-c_row)*cos(a)) + c_row);
  return(get_pix(i_img, col, row));

}


int swirl(Image * o_img, Image * i_img, int c_col, int c_row, int scale)
{
  //Define new array of pixels with proper dimensions
  Pixel * resized = malloc(sizeof(Pixel) * i_img->sz[0] * i_img->sz[1]);
  if (resized==NULL)
    return 8;
  //Loop through this new array and set each new pixel to the corresponding old pixel based on ouptut of getSwirlPix
  for (int i = 0; i<i_img->sz[1]; i++)
    {
      for (int j = 0; j<i_img->sz[0]; j++)
	{//Calculate location of corresponding old pixel
	    if (getSwirlPix(i_img,c_col,c_row,j,i,scale)!=NULL)
	      {
	        resized[i*(i_img->sz[0])+j] = *(getSwirlPix(i_img, c_col, c_row, j, i, scale));
	      }
	    else
	      {//If pixel out of bounds, set to black
	        set_rbg(&(resized[i*(i_img->sz[0])+j]),0,0,0);
	      }
	}
    }
  //Set o_img to correct data and dimensions
  o_img->data = resized;
  o_img->sz[0] = i_img->sz[0];
  o_img->sz[1] = i_img->sz[1];
  return 0;
}

double get_gauss(int d_x, int d_y, float coef)
{ // Given d_x, and coef, generates value from gauss formula.
	// double e_term = 
	double g = (1.0 / (2.0 * PI * pow(coef, 2))) * 
		exp(-(pow(d_x, 2) + pow(d_y, 2)) / (2 * pow(coef, 2)));
	return g;
}

void fill_gauss(double *matrix, int size, float coef)
{ // Given a square matrix, populates it with gauss values. 
	for(int d_c=-size/2; d_c < size/2 + 1; ++d_c){
		for(int d_r=-size/2; d_r < size/2 + 1; ++d_r){
			matrix[(d_r + size/2) * size + d_c + size/2] = get_gauss(d_c, d_r, coef);
			// printf(" %f ", get_gauss(d_c, d_r, coef));
		}
		// printf("\n");
	}

}


void blur_pix(Pixel * pix, int col, int row,Image * i_img, int size, int h_size,double * matrix, int * c_b, int * r_b)
{ // For each individual pixel, it is blurred, given a gauss matrix.
	double g_sum = 0;
	double rgb_buf[3] = {0}; // Float buffer, so val not rounded to 0 when casted to char.
	double cur_coef = 0;
	
	// printf("\nCOL: %d ROW: %d\n%d %d\n%d %d\n", 
	// col, row, c_b[0], c_b[1],r_b[0], r_b[1]);
	// Loops through the g_matrix 
	for(int d_r=r_b[0]; d_r < r_b[1]; ++d_r){
		for(int d_c=c_b[0]; d_c < c_b[1]; ++d_c){
			// Pixel *cur_src = get_pix_d(i_img, col, row,	d_c-h_size, d_r-h_size);
			// if (cur_src != NULL){} // Old code no longer used for efficiency.
			Pixel *cur_src = &(i_img->data[(row+d_r-h_size)*i_img->sz[0] + (col+d_c-h_size)]);

			// Loops through r,g,b of a pixel and adds to a float buffer.
			cur_coef = matrix[d_c + size * d_r];
			rgb_buf[0] += cur_src->r * cur_coef;
			rgb_buf[1] += cur_src->g * cur_coef;
			rgb_buf[2] += cur_src->b * cur_coef;
			g_sum += cur_coef; // Gauss sum, used to normalise values	
		}
	}
	// Normalise Gauss sum and store it in pix.
	pix -> r = rgb_buf[0] / g_sum;
	pix -> g = rgb_buf[1] / g_sum;
	pix -> b = rgb_buf[2] / g_sum;
}

int blur(Image * o_img, Image * i_img, float coef)
{ 	
	if(coef < 0) // If coef is less than 0, there should be an error
		return 6;
	if(0 != init_data(o_img, i_img -> sz[0], i_img -> sz[1]))
		return 8;
	// size of the matrix, which will always be odd
	int size = (int)(coef * 10) + (1 - (int)(coef * 10) % 2); // if even, add 1.
	int h_size = size / 2;
	// g_matrix is used to store the gauss matrix values, will be freed afterwards.
	double * g_matrix = calloc(size * size, sizeof(double));
	// printf("Size: %d  Coef: %f\n", size, coef);
	fill_gauss(g_matrix, size, coef);
	// loops through each pixel and blurs it with blur_pix function.
	for(int row=0; row<o_img->sz[1]; ++row){
		// boundaries are calculated, no need for boundary checks when retrieving pixels.
		int r_b[2] = {0, size}; 
		if((i_img->sz[1] - row - 1) < h_size)
			r_b[1] = i_img->sz[1] - row + h_size;
		if(row < h_size)
			r_b[0] = h_size - row;
		
		for(int col=0; col<o_img->sz[0]; ++col){
			int c_b[2] = {0, size}; // boundaries are calculated
			if((i_img->sz[0] - col - 1) < h_size)
				c_b[1] = i_img->sz[0] - col + h_size;
			if(col < h_size)
				c_b[0] = h_size - col;
			
			double g_sum = 0;
			double rgb_buf[3] = {0}; // Float buffer, so val not rounded to 0 when casted to char.
			double cur_coef = 0;
			Pixel * pix = &(o_img -> data[col + row * o_img -> sz[0]]);
			
			// printf("\nCOL: %d ROW: %d\n%d %d\n%d %d\n", 
			// col, row, c_b[0], c_b[1],r_b[0], r_b[1]);
			// Loops through the g_matrix 
			for(int d_r=r_b[0]; d_r < r_b[1]; ++d_r){
				for(int d_c=c_b[0]; d_c < c_b[1]; ++d_c){
					// Pixel *cur_src = get_pix_d(i_img, col, row,	d_c-h_size, d_r-h_size);
					// if (cur_src != NULL){} // Old code no longer used for efficiency.
					Pixel *cur_src = &(i_img->data[(row+d_r-h_size)*i_img->sz[0] + (col+d_c-h_size)]);

					// Loops through r,g,b of a pixel and adds to a float buffer.
					cur_coef = g_matrix[d_c + size * d_r];
					rgb_buf[0] += cur_src->r * cur_coef;
					rgb_buf[1] += cur_src->g * cur_coef;
					rgb_buf[2] += cur_src->b * cur_coef;
					g_sum += cur_coef; // Gauss sum, used to normalise values	
				}
			}
			// Normalise Gauss sum and store it in pix.
			pix -> r = rgb_buf[0] / g_sum;
			pix -> g = rgb_buf[1] / g_sum;
			pix -> b = rgb_buf[2] / g_sum;
			
			/* blur_pix(get_pix(o_img,col,row), col, row, i_img, size, h_size, g_matrix, c_b, r_b);*/
		}
	}
	free(g_matrix); // Frees g_matrix once operation finished
	return 0;
}

void clear(FILE * pointers[], int p,  Image *images[], int im)
{
  for (int i = 0; i< p; i++)
    {
      fclose(pointers[i]);
    }

  for (int j = 0; j< im; j++)
    {
      rmv_img(images[j]);
    }
}

int clear_error(FILE * pointers[], int p,  Image *images[], int im, char *err, int num)
{
  for (int i = 0; i< p; i++)
    {
      fclose(pointers[i]);
    }

  for (int j = 0; j< im; j++)
    {
      rmv_img(images[j]);
    }

  fprintf(stderr,"%s",err);
  return num;
}

//---------------------------------------------------------------------------------------------------
//WORKS WITH INPUTTED COMMAND LINE ARGUMENTS; EXECUTES OPERATION SPECIFIED OR RETURNS ERROR:

int run_imageManip(int argc, char *argv[])
{
  //Checking if input and output files were specified
  if (argc<=2)
    {
      fprintf(stderr,"Failed to supply input filename or output filename, or both\n");
      return 1;
    }

  //Opening input file
  FILE * input = fopen(argv[1], "rb");
  if (input == NULL)
    {
      fprintf(stderr,"Specified input file could not be opened\n");
      return 2;
    }
  
  //Create arrays which store which pointers and images that have been opened and must be closed to avoid VALGRIND errors
  FILE * openp[3];
  Image * openim[3];

  //p and im track how many indices of openp and openim have been filled
  int p = 0;
  int im = 0;

  openp[0] = input;
  p++;

  //Int which stores whether a malloc error has occurred
  int erroreight = 0;
  
  //Create i_img
  Image * i_img = new_img();

  if (i_img ==NULL)
    {
      erroreight = 1;
    }
  else
    {
      openim[0] = i_img;
      im++;
    }

  //Reading into i_img
  int inputread = read_ppm(input, i_img);
  if(inputread==0)
    {
      printf("The input img size is: \n%d cols. \n%d rows. \n", i_img->sz[0], i_img->sz[1]);
    }
  else
   {
     if (erroreight !=0)
       {
	 return (clear_error(openp, p, openim, im, "Reading input fails due to malloc error while creating i_img object\n",3));
       }
     else
       {
	 return (clear_error(openp, p, openim, im, "Error reading input for i_img\n", 3));
       }
   }

  //Creating output image
  Image * o_img = new_img();
  if (o_img ==NULL)
    {
      erroreight = 1;
    }
  else
    {
      openim[im] = o_img;
      im++;
    }
  
  //Checking if operation name was specified
  if (argc<=3)
    {
      return (clear_error(openp, p, openim, im, "No operation specified\n", 4));
    }

  //MATCHING FOR NAME OF OPERATION
  if (strcmp("exposure",argv[3])==0) //EXPOSURE
    {
      if(argc!=5)
	{
	  return (clear_error(openp, p, openim, im, "Incorrect number of args\n",5));
      	}
      float a;
      int scanned = sscanf(argv[4], "%f", &a);
      if ((scanned!=1) | (a<-3) | (a>3))
	{
	  return (clear_error(openp, p, openim, im, "Arguments out of range or senseless\n", 6));
	}
      if (erroreight==0 && exposure(o_img,i_img,a)!=0)
	{
	  erroreight = 1;
	}
    }
  else if (strcmp("blend", argv[3])==0) //A_BLEND
    {
      if (argc!=6)
	{
	  return (clear_error(openp, p, openim, im, "Incorrect number of args \n", 5));
	}
      FILE *input2 = fopen(argv[4], "rb");

      if (input2!=NULL)
	{
	  openp[p] = input2;
	  p++;
	}
      else
	return (clear_error(openp, p, openim, im, "Specified file could not be opened\n", 2));

      Image *i_img_2 = new_img();

      if (i_img_2!=NULL)
	{
	  openim[im] = i_img_2;
	  im++;
	}
      else
	{
	  erroreight = 1;
	}

      int secondinputread = read_ppm(input2,i_img_2);
      if (secondinputread == 0)
	printf("The second img size is: \n%d cols. \n%d rows. \n", i_img->sz[0], i_img->sz[1]);
      else
	{
	  return (clear_error(openp, p, openim, im, "Problem reading second input\n", 3));
	}
      float scale;
      int scanned = sscanf(argv[5], "%f", &scale);
      if ((scanned!=1) |(scale < 0) | (scale > 1))
	{
	  return (clear_error(openp, p, openim, im, "Scale value invalid\n", 6));
	}
      
      if (erroreight == 0 && a_blend(o_img,i_img,i_img_2,scale)!=0)
	{
	  erroreight = 1;
	}
    }
  else if (strcmp("zoom_in", argv[3])==0) //ZOOM IN
    {
      if (argc!=4)
	{
	  return (clear_error(openp,p, openim, im, "Incorrect number of arguments \n", 5));
	}
      if (erroreight ==0 && zoom_in(o_img,i_img)!=0)
	{
	  erroreight = 1;
	}
    }
  else if (strcmp("zoom_out",argv[3])==0) //ZOOM OUT
    {
      if (argc!=4)
	{
	  return (clear_error(openp,p, openim, im, "Incorrect number of arguments \n", 5));
	}
      if (erroreight == 0 && zoom_out(o_img,i_img)!=0)
	{
	  erroreight = 1;
	}
    }
  else if (strcmp("pointilism",argv[3])==0) //POINTILISM
    {
      if (argc!=4)
	{
	  return (clear_error(openp,p, openim, im, "Incorrect number of arguments \n", 5));
	}
      if (erroreight ==0 && pointilism(o_img,i_img)!=0)
	{
	  erroreight = 1;
	}
    }
  else if (strcmp("swirl",argv[3])==0) //SWIRL
    {
      if (argc!=7)
	{
	  return (clear_error(openp,p, openim, im, "Incorrect number of arguments \n", 5));
	}
      int c_col, c_row, scale;
      char c;
      int scannedcol = sscanf(argv[4], "%d", &c_col);
      int scannedrow = sscanf(argv[5], "%d", &c_row);
      int scannedscale = sscanf(argv[6], "%d%c", &scale, &c);
      printf("Col: %d, Row: %d", c_col, c_row);
      printf("Scale: %d\n", scale);

      if (scannedscale==2)
	{
	  return (clear_error(openp, p, openim, im, "Scale must be an integer \n", 6));
	}
      
      if ((scannedcol!=1) | (scannedrow!=1) | (scannedscale!=1))
	{
	  return (clear_error(openp, p, openim, im, "Arguments invalid/senseless \n", 6));
	}

      if ((c_col > i_img->sz[0]) | (c_row > i_img->sz[1]) | (scale<= 0) | (c_col < 0) | (c_row < 0))
	{
	  return (clear_error(openp, p, openim, im, "Arguments out of range or senseless \n", 6));
	}

      printf("Col: %d, Row: %d, Scale: %d\n", c_col,c_row,scale);
      if (erroreight == 0 && swirl(o_img,i_img,c_col,c_row, scale)!=0)
	{
	  erroreight = 1;
	}
    }
  else if (strcmp("blur", argv[3])==0) //BLUR
    {
      if (argc!=5)
	{
	  return (clear_error(openp,p, openim, im, "Incorrect number of arguments \n", 5));
	}
      float coef;
      int scanned = sscanf(argv[4],"%f", &coef);
      if ((scanned!=1) | (coef<0))
	{
	  return (clear_error(openp, p, openim, im, "Argument for operation out of range or invalid\n", 6));
	}
      
      if(erroreight == 0 && blur(o_img, i_img, coef)!=0)
	{
	  erroreight = 1;
	}
    }
  else //NO OPERATION NAME MATCHED
    {
      return (clear_error(openp, p, openim, im, "No operation name was specified or operation name was invalid \n", 4));
    }

  //WRITING OUTPUT
  FILE * outputwrite = fopen(argv[2], "wb");
  if (outputwrite == NULL)
    {
      return (clear_error(openp, p, openim, im, "Could not open output file \n", 7));
    }

  openp[p] = outputwrite;
  p++;

  int written = write_ppm(outputwrite, o_img);
  if (written!=0)
    {
      if (written==8)
	{
	  return (clear_error(openp, p, openim, im, "Writing output fails due to malloc error \n", 7));
	}
      else
	{
	  return(clear_error(openp, p, openim, im, "Error in writing output file \n", 7));
	}
    }

  if (erroreight!=0)
    {
      return (clear_error(openp, p, openim, im, "Dynamic allocation failed\n",8));
    }
  
  
  clear(openp, p, openim, im);
  return 0;
}
