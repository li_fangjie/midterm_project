CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g

project: project.o ppm_io.o imageManip.o
	$(CC) project.o ppm_io.o imageManip.o -o project -lm

project.o: project.c
	$(CC) $(CFLAGS) -c project.c

imageManip.o: imageManip.c imageManip.h 
	$(CC) $(CFLAGS) -c imageManip.c 

ppm_io.o: ppm_io.c ppm_io.h
	$(CC) $(CFLAGS) -c ppm_io.c

clean:
	rm -f *.o project 
