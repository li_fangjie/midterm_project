// Sabrina Li
// sli159
// Fangjie Li
// fli35

// __Add your name and JHED above__
// ppm_io.h
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file


#ifndef PPM_IO_H
#define PPM_IO_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* A struct to store a single RGB pixel, one byte per color channel.
 */
typedef struct _pixel {
  unsigned char r;  //red   channel value
  unsigned char g;  //green channel value
  unsigned char b;  //blue  channel value
} Pixel;


/* A struct to bundle together a pixel array with the other
 * image data we'll frequently want to pass around with it.
 * (This saves us from having to pass the same three 
 * variables to every function.) Note that no Pixels are
 * stored within this struct; the data field is a pointer.
 */
typedef struct _image {
	Pixel *data;  //pointer to array of Pixels
	int sz[2]; // sz[0] is cols, sz[1] is rows
	// int rows;     //number of rows of Pixels
	// int cols;     //number of columns of Pixels
} Image;




/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
int read_ppm(FILE *fp, Image * img);


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im);


Image * new_img();

Image * init_img(int col, int row);

int cpy_img(Image * des, Image * src);

int rmv_img(Image * img);

int init_data(Image * img, int col, int row);

#endif
